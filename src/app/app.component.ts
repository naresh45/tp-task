import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tp';

  companies=[
    {
      identificationNumber:'900674335',
      companyName:'TP Company X',
      identificationType:'N',
      firstName:'TP',
      firstLastName:'Company',
      address:'',
      canRegister:false,
      reason:'Company Already Registered'
    },
    {
      identificationNumber:'900674336',
      companyName:'TP Company A',
      identificationType:'',
      firstName:'TP',
      firstLastName:'Company',
      address:'',
      canRegister:true
    },
    {
      identificationNumber:'811033098',
      companyName:'TP Company B',
      identificationType:'',
      firstName:'TP',
      firstLastName:'Company',
      address:'',
      canRegister:true
    }
  ];
  constructor(){
    localStorage.setItem('companies',JSON.stringify(this.companies));
  }
}
