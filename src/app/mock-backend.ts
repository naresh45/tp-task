import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

// let companiesData = localStorage.getItem('companies') || '';
// let companies = companiesData ? JSON.parse(companiesData) : [];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        // wrap in delayed observable to simulate server api call
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {
                case url.endsWith('/check-nit') && method === 'POST':
                    return checkNit();
                case url.endsWith('/register') && method === 'POST':
                    return register();
                default:
                    // pass through any requests not handled above
                    return next.handle(request);
            }
        }

        function register() {
            const user = body
            return ok(body);
        }

        function checkNit() {
            let companiesData = localStorage.getItem('companies');
            let companies = companiesData ? JSON.parse(companiesData) : [];
            let data = companies.find((x: any) => body.nit === x.identificationNumber);
            return data && data.canRegister ? ok(data) : error(data ? data.reason : 'Identification Number Does Not Exist');
        }

        // helper functions

        function ok(body?: object) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function error(message?: string) {
            return throwError({ error: { message } });
        }
    }
}

export const fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};