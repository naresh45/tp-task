import { HttpClient } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';

export class FormErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && isSubmitted);
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('messageBox') messageBox: TemplateRef<any>;
  firstFormGroup: FormGroup;
  registrationForm: FormGroup;
  isEditable = false;
  nitValue = new FormControl('');
  errorState = new FormErrorStateMatcher();
  dialogData = {
    type:'',
    message:''
  };
  types=[
    'NIT',
    'NON NIT',
    'OTHERS'
  ];

  constructor(private _formBuilder: FormBuilder, public http: HttpClient,public dialog: MatDialog) { }

  ngOnInit() {
    this.registrationForm = this._formBuilder.group({
      identificationNumber: [''],
      companyName: [''],
      identificationType: [''],
      firstName: [''],
      secondName: [''],
      firstLastName: [''],
      secondLastName: [''],
      address: [''],
      municipality: [''],
      email: ['', [Validators.email]],
      phone: [''],
    });
  }

  checkRegister() {
    this.http.post('/path/check-nit', { nit: this.nitValue.value }).subscribe((response:any) => {
      this.stepper.next();
      this.registrationForm.patchValue(response);
      this.dialogData['type']=response.identificationNumber;
      this.dialogData['message']=`Company Name: ${response.companyName}`;
      this.dialog.open(this.messageBox);
    }, error => {
      this.dialogData['type']='Error';
      this.dialogData['message']=error.error.message;
      this.dialog.open(this.messageBox);
      console.log(error)
    })
  }

  register(){
    if(!this.registrationForm.valid){
      this.dialogData['type']='Error';
      this.dialogData['message']='Please Fill the Mandatory fields';
      this.dialog.open(this.messageBox);
      return;
    }
    this.http.post('/path/register',this.registrationForm.value).subscribe((response:any) => {
      this.dialogData['type']=response.identificationNumber;
      this.dialogData['message']=`${response.companyName} Registered Successfully`;
      this.dialog.open(this.messageBox);
    }, error => {
      this.dialogData['type']='Error';
      this.dialogData['message']=error.error.message;
      this.dialog.open(this.messageBox);
      console.log(error)
    })
  }

}
